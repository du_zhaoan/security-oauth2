package cn.edu.dgut.sw.lab.security.oauth2.config.annotation;

import cn.edu.dgut.sw.lab.security.oauth2.config.SaiOAuth2ClientRegistrationRepositoryConfiguration;
import cn.edu.dgut.sw.lab.security.oauth2.config.SaiOAuth2LoginSecurityConfigurer;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AliasFor;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import java.lang.annotation.*;

@SuppressWarnings("unused")
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SaiOAuth2ClientRegistrationRepositoryConfiguration.class, SaiOAuth2LoginSecurityConfigurer.class})
@EnableWebSecurity
public @interface EnableSaiOAuth2Login {

    /**
     * 定义debug属性，作为元注解{@link EnableWebSecurity}的debug属性的别名
     * Alias for the {@link EnableWebSecurity#debug()} attribute. Allows for more concise annotation declarations e.g.:
     *
     * @return if true, enables debug support with Spring Security
     * @since 1.0.5
     */
    @AliasFor(annotation = EnableWebSecurity.class, attribute = "debug")
    boolean debug() default false;
}
